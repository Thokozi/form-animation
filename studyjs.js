function animatedForm() {
    /**
     *sets a constant variable called arrows which
     * selects all elements with the class name fa-arrow-down
     */
    const arrows = document.querySelectorAll('.fa-arrow-down'); //document.querySelectorAll - returns all elements in the document that matches the specified CSS selector(s), as a static NodeList object.

    /**
     * For each element within arrows we perform an action
     */
    arrows.forEach(arrow => { //calls a function once for each element in an array, in order
        arrow.addEventListener('click', () => {//When the arrow object is clicked, perform the following actions
            const input = arrow.previousElementSibling; //input variable gets the previous element of the arrow element, in this case the input element
            const parent = arrow.parentElement; // get parent element of arrow, in this case the parent div class
            const nextForm = parent.nextElementSibling; // gets next element from the parent element, in this case the next div element

            // check for validation
            if(input.type === "text" && validateUser(input)) {// if the input type is text and the validateUser function returns true
               nextSlide(parent,nextForm);
            } else if (input.type === 'email' && validateEmail(input)){// if the input type is email and the validateEmail function returns true
                nextSlide(parent, nextForm);
            } else if (input.type === 'password' && validateUser(input)){// if the input type is password and the validateUser function returns true
                nextSlide(parent, nextForm);
            }else {
                parent.style.animation = 'shake 0.5s ease'; // add a style animation to the parent div
            }
            //get rid of animation
            parent.addEventListener("animationend", () =>{// animationend is fired when the CSS animation has completed
                parent.style.animation= '';
            });
        });
    });
}

function nextSlide(parent, nextForm) {
    parent.classList.add('inactive');//add class inactive to current parent div
    parent.classList.remove('active');//remove class active from current parent div
    nextForm.classList.add('active');//add class active to next div element

}

function validateUser(user){
    if(user.value.length < 6){
        console.log('not enough characters');
        error('rgb(189,87,87)');

    }else{
        error('rgb(87,189,130)');
        return true;
    }
}

function validateEmail(email) {
    const validation = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (validation.test(email.value)){//test() - tests for a match in a string
        error('rgb(87,189,130)');
        return true;
    }else {
        error('rgb(189,87,87)');
    }
}
function error(color){
    document.body.style.backgroundColor = color;
}
animatedForm();